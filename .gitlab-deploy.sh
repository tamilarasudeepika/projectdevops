#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVERS
echo "$string"
array=(${string//,/})
echo "$array"
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do    
  echo "Deploy project on server ${array[i]}"    
  ssh ubuntu@${array[i]} "cd /var/www/ex1.underdev.in/public_html && sudo git pull origin master"
  done
